%%
%Author: Sandeep MANANDHAR
%contact: sandeep.manandhar@inria.fr
%Inria, Rennes; Team Serpico
%About the code:
% This code provides WSAE error as described in the main paper.

%  
% Usuage:
% requires u, v, w scalar fields in tiff file format as input
% requires source, target and source mask file
% output ASAE error on matlab console
% Quiver plot of structure arrows
clear all;


%%Change the file path below

sourcepath = '/home/smanandh/Desktop/128/frame_T0.ome.tif';
targetpath = '/home/smanandh/Desktop/128/frame_T1.ome.tif';
sourcemaskpath = '/home/smanandh/Desktop/128/mask_T0.ome.tif';

upath = '/home/smanandh/mount/c++/withTV/build-CTvariational-Desktop_Qt_5_7_0_GCC_64bit-Default/frame_T0.ome.tif_frame_T1.ome.tifsoru.tif';
vpath = '/home/smanandh/mount/c++/withTV/build-CTvariational-Desktop_Qt_5_7_0_GCC_64bit-Default/frame_T0.ome.tif_frame_T1.ome.tifsorv.tif';
wpath ='/home/smanandh/mount/c++/withTV/build-CTvariational-Desktop_Qt_5_7_0_GCC_64bit-Default/frame_T0.ome.tif_frame_T1.ome.tifsorw.tif';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u = readtiff(upath);
v = readtiff(vpath);
w = readtiff(wpath);

source=readtiff(sourcepath);
target =readtiff(targetpath);



mask = readtiff(sourcemaskpath);
mask = imresize3(mask, size(u), 'nearest');

source = imresize3(source, size(u), 'cubic');
target = imresize3(target, size(u), 'cubic');


mask(mask>0) = 1;

u = medfilt3(u, [3,3,3]);
v = medfilt3(v, [3,3,3]);
w = medfilt3(w, [3,3,3]);

uu = u.*mask; vv= v.*mask;
ww= w.*mask;

%%
target = imresize3(target, size(u));
target = medfilt3(target, [3,3,3]);
target = (target - min(target(:)))/(max(target(:)) - min(target(:)));


source = imresize3(source, size(u));% size(u));
source = medfilt3(source, [3,3,3]);
source = (source - min(source(:)))/(max(source(:)) - min(source(:)));


D(:,:,:,1) = u;
D(:,:,:,2) = v;
D(:,:,:,3) = w;

wI =imwarp(target, D);
wI = (wI - min(wI(:)))/(max(wI(:)) - min(wI(:)));
source = (source - min(source(:)))/(max(source(:)) - min(source(:)));
%%

[ge1 ge2 ge3 gev1 gev2 gev3] = computeTensors(source, [3,3,3]);


[r c p] = size(ge1);


%%
[e1 e2 e3 ev1 ev2 ev3] = computeTensors(wI, [3,3,3]);

ue1 = e1;
ue2 = e2;
ue3 = e3;

uge1 = ge1;
uge2 = ge2;
uge3  =ge3;

%%
e1(mask<1) = nan;
e2(mask<1) = nan;
e3(mask<1) = nan;

ge1(mask<1) = nan;
ge2(mask<1) = nan;
ge3(mask<1) = nan;

gev1(mask<1) = nan;
%%
ngev1 = gev1(10:end-10, 10:end-10, 5:end-5);


ang = nan(size(e1));
[rr cc pp] = size(e1);
for i =1:rr
    for j =1:cc
        for k=1:pp
            
            v0 = [e1(i,j,k), e2(i,j,k),  e3(i,j,k)];
            v1 = [ge1(i,j,k), ge2(i,j,k),  ge3(i,j,k)];
            
            
            v0 = v0/norm(v0);
            v1 = v1/norm(v1);
            
            anglerad =acos(   dot(v0,v1 )   );
            ang(i,j,k) =  min(anglerad,  abs(pi-anglerad)   );
            ang2(i,j,k) = anglerad;

           
            
        end
    end
end
disp('WSAE error: ')
nansum(ang(:))./sum(~(isnan(ang(:))))

%%
ue1(mask<1)  = nan;
ue2(mask<1)  = nan;
ue3(mask<1)  = nan;

uge1(mask<1)  = nan;
uge2(mask<1)  = nan;
uge3(mask<1)  = nan;


%%

[r c p] = size(e1);
figure
[X Y Z] = meshgrid(1:c, 1:r, 1:p);
sp= 3;
z = 12;

imshow(source(:,:,z),[],'colormap', jet);
hold on;

quiver(X(1:sp:r, 1:sp:c,z),Y(1:sp:r, 1:sp:c,z),(e1(1:sp:r, 1:sp:c,z)),  (e2(1:sp:r, 1:sp:c,z)),...
    'AutoScale' , 'on', 'color','g');
quiver(X(1:sp:r, 1:sp:c,z),Y(1:sp:r, 1:sp:c,z),(ge1(1:sp:r, 1:sp:c,z)),  (ge2(1:sp:r, 1:sp:c,z)),...
    'AutoScale' , 'on', 'color','r');

title('in red: ground truth ; in green: computed structure');

%%

%%
ww = abs(wI - source);






