function u = readtiff(filename)

imi = imfinfo(filename);
for i =1:numel(imi)
    u(:,:,i) = im2double(imread(filename, i));
end