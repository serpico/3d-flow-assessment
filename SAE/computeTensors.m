%%
%Author: Sandeep MANANDHAR
%contact: sandeep.manandhar@inria.fr
%Inria, Rennes; Team Serpico

function [eigVec1, eigVec2, eigVec3, eigval1, eigval2, eigval3] = computeTensors(I1, sig)


[~, ~, ~, Fx2, Fy2, Fz2, Fxy, Fxz, Fyz] = getAutoCorrComponent(I1, sig);
% I1 = mat2gray(I1);
% [Fx Fy Fz] = imgradientxyz(I1);
% Fx = imgaussfilt3(Fx,5);
% Fy = imgaussfilt3(Fy,5);
% Fz = imgaussfilt3(Fz,3);


[r c p] = size(I1);

eigVec1 = zeros(size(I1));
eigVec2 = zeros(size(I1));
eigVec3 = zeros(size(I1));
eigval = zeros(size(I1));
parfor i=1:r
    for j=1:c
        for k = 1:p
            
            H = [Fx2(i,j,k) Fxy(i,j,k) Fxz(i,j,k);
                Fxy(i,j,k) Fy2(i,j,k) Fyz(i,j,k);
                Fxz(i,j,k) Fyz(i,j,k) Fz2(i,j,k)];
            
%             H = [Fx(i,j,k)*Fx(i,j,k) Fx(i,j,k)*Fy(i,j,k) Fx(i,j,k)*Fz(i,j,k);
%                     Fx(i,j,k)*Fy(i,j,k) Fy(i,j,k)*Fy(i,j,k) Fy(i,j,k)*Fz(i,j,k);
%                     Fx(i,j,k)*Fz(i,j,k) Fz(i,j,k)*Fy(i,j,k) Fz(i,j,k)*Fz(i,j,k)];
            [e v] = eig(H * sign(H(1)));
            
            eigval1(i,j,k) = v(1,1);
            eigval2(i,j,k) = v(2,2);
            eigval3(i,j,k) = v(3,3);
            
            
            eigVec1(i,j,k) = e(1,1);
            eigVec2(i,j,k) = e(2,1);
            eigVec3(i,j,k) = e(3,1);
        end
    end
end