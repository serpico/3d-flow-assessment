# 3D flow assessment

Author : Sandeep MANANDHAR

contact: sandeep.manandhar@inria.fr

Inria, Rennes; Team Serpico

Directory structure:

3DHSV: Files required to create visualization with 3DHSV method.
Requires u,v,w scalar components as input in tif format.
Generates a 3D RGB tiff file. Use fiji/ImageJ or similar software to view the 3D image.

3PHS: Files required to create visualization with 3PHS method
Requires u,v,w scalar components as input in tif format.
Requires (x,y,z) of point to specify 3 planes.
Generates an image in matlab. Use matlab imshow to view the 2D visualization.


SAE: Files required to compute WSAE measure
Requires u,v,w scalar components as input in tif format.
Requires source, target and sourcemask as input in tif format.
May take some time to compute eigencomponents of strucuture tensor (twice).
See matlab console at the end to see the WSAE measure.
Additionally generates a 3D quiver plot of a plane visualizing eigenvectors.


data: Sample source, target and source mask files. You may use these files to compute 3D flow fields using 3DPatchMatch + our variational method.
