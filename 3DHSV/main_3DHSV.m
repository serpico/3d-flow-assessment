%%
%Author: Sandeep MANANDHAR
%contact: sandeep.manandhar@inria.fr
%Inria, Rennes; Team Serpico
%About the code:
% This code provides 3DHSV visualization as described in the main paper.

%  
% Usuage:
% requires u, v, w scalar fields in tiff file format as input
% set the filepath 
% output is a 3d rgb tif stack file



%%Change the file path below
upath = '/home/smanandh/mount/c++/withTV/build-CTvariational-Desktop_Qt_5_7_0_GCC_64bit-Default/frame_T0.ome.tif_frame_T1.ome.tifsoru.tif';
vpath = '/home/smanandh/mount/c++/withTV/build-CTvariational-Desktop_Qt_5_7_0_GCC_64bit-Default/frame_T0.ome.tif_frame_T1.ome.tifsorv.tif';
wpath ='/home/smanandh/mount/c++/withTV/build-CTvariational-Desktop_Qt_5_7_0_GCC_64bit-Default/frame_T0.ome.tif_frame_T1.ome.tifsorw.tif';
%%%%%%%%%%%%%%%%%%%%%%%%%%%

uu = readtiff(upath);
vv = readtiff(vpath);
ww = readtiff(wpath);

%% To be set between 0 and 1
% Controls the value parameter; tune it according to your ability to see
% dark and bright pixels
valueMAX = .8;
valueMIN = .2;




%     uu = 2*imresize3(uu, [2*size(uu,1), 2*size(uu,2), 2*size(uu,3)]);
%     vv= 2*imresize3(vv, size(uu));
%     ww = 2*imresize3(ww, size(uu));


rad = sqrt(uu.^2 + vv.^2+ww.^2 );
a = atan2(-vv, -uu)/pi*180+180; %between 0-360

hue = mod((a - min(a(:)))./(max(a(:))-min(a(:)))-.08,1);  %normalizing between 0-1
rad(isnan(rad)) = 0;
rad = (rad - min(rad(:)))./(max(rad(:)) - min(rad(:)));
V = (ww - min(ww(:)))./(max(ww(:))-min(ww(:)));



upplane = zeros(size(ww));
downplane = zeros(size(ww));

upplane(ww>=0) = ww(ww>=0);
downplane(ww<0) = ww(ww<0);


upplane = (valueMAX-0.5)*(upplane-min(upplane(:)))./(max(upplane(:)) - min(upplane(:))) + 0.5;
downplane = (0.499-valueMIN)*(downplane-min(downplane(:)))./(max(downplane(:)) - min(downplane(:))) + valueMIN;
V(ww>=0) = upplane(ww>=0);
V(ww<0) = downplane(ww<0);

ii = [];
filename = ['output' '.tif'];
if exist(filename, 'file') == 2
    delete(filename);
end
for i=1:size(uu,3)
    ii(:,:,1) = hue(:,:,i);
    ii(:,:,2) = rad(:,:,i);
    ii(:,:,3) = V(:,:,i);
    img = hsv2rgb(ii);
    
    imwrite(img, filename,  'WriteMode', 'append');
    
end