%%
%Author: Sandeep MANANDHAR
%contact: sandeep.manandhar@inria.fr
%Inria, Rennes; Team Serpico
%About the code:
% This code provides 3PHS visualization as described in the main paper.
% The code partly uses functions of 
% Deqing Sun, Department of Computer Science, Brown University
% (dqsun@cs.brown.edu)
%  
% Usuage:
% requires u, v, w scalar fields in tiff file format as input
% requires a (x,y,z) location to create 3 orthogonal planes
% output is a 2d rgb image 

%%
%Input scaler field tif files


%%Change the file path below
upath = '/media/smanandh/Sandeep/paper_code/build-CTvar-Desktop_Qt_5_7_0_GCC_64bit-Default/1_CAM01_000008.tif_1_CAM01_000012.tifsoru.tif';
vpath = '/media/smanandh/Sandeep/paper_code/build-CTvar-Desktop_Qt_5_7_0_GCC_64bit-Default/1_CAM01_000008.tif_1_CAM01_000012.tifsorv.tif';
wpath ='/media/smanandh/Sandeep/paper_code/build-CTvar-Desktop_Qt_5_7_0_GCC_64bit-Default/1_CAM01_000008.tif_1_CAM01_000012.tifsorw.tif';
%%%%%%%%%%%%%%%%%%%%%%%%%%


u = readtiff(upath);
v = readtiff(vpath);
w = readtiff(wpath);

%Choose a point in all three planes
x = floor(size(u,2)/2);
y = floor(size(u,1)/2);
z = 17;floor(size(u,3)/2);

%%
%(x,y,z) is the location of the point intersected by 3 planes
% 7th argument is normaliation amplitude of the motion
% -1 sets it to default maximum of the flow field
% 8th argument is 0 to NOT label the planes
% set it to 1 to label the planes
img = flow3projections(u,v,w, x,y,z,-1);

%Display the results
imshow(img)
