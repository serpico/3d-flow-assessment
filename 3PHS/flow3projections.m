function img = flow3projections(uu,vv,ww, x,y,z, ff)


%  z =	108; x =98; y =134;
%   z =	91; x =45; y =45;
%   z =	100/2; x =184/2; y =142/2;
% z=27, x =size(uu,2)/2, y = size(uu,1)/2;


% if normalize==1
%     m = sqrt(uu.^2+vv.^2+ww.^2);
%     uu= uu./m;
%     vv = vv./m;
%     ww = ww./m;
% end
uxz = (reshape(uu(y,:, :), [size(uu,2), size(uu,3)])); 
uxy = reshape(uu(:,:,z), [size(uu,1), size(uu,2)]);
uyz = (reshape(uu(:,x,:), [size(uu,1 ), size(uu,3)]));

vxz = (reshape(vv(y,:, :), [size(vv,2), size(vv,3)])); 
vxy = reshape(vv(:,:,z), [size(vv,1), size(vv,2)]);
vyz = (reshape(vv(:,x,:), [size(vv,1 ), size(vv,3)]));

wxz = (reshape(ww(y,:, :), [size(ww,2), size(ww,3)])); 
wxy = reshape(ww(:,:,z), [size(ww,1), size(ww,2)]);
wyz = (reshape(ww(:,x,:), [size(ww,1 ), size(ww,3)]));

if legend==1
ccode = imread('flow_color.png'); %in MATLAB folder
else
    ccode = 1;
end
    
clear flowXY; clear flowZY; clear flowXZ;
flowXY(:,:,1) = uxy; 
flowXY(:,:,2) = vxy;
imgxy = flowToColor(flowXY,ff);

flowXZ(:,:,1) = uxz'; 
flowXZ(:,:,2) = wxz';
imgxz = (flowToColor(flowXZ,ff));


flowZY(:,:,1) = wyz; 
flowZY(:,:,2) = vyz;
imgzy = flowToColor(flowZY,ff);

imgxy(:, x, 1) = 255;    
imgxy(:, x, 2:3) = 0;     
imgxy(y, :, 1) = 255;   
imgxy(y, :, 2:3) = 0;    

 imgxy(:, 1,:) = 0;
 imgxy(1, :,:) = 0;


imgxz(:, x, 1) = 255;
imgxz(:, x, 2:3) = 0;
imgxz(z, :, 1) = 255;
imgxz(z, :, 2:3) = 0;

 imgxz(:, 1,:) = 0;
 imgxz(1, :,:) = 0;
  imgxz(end, :,:) = 0;
 imgxz(:, end,:) = 0;


imgzy(:,z, 1) = 255;
imgzy(:,z, 2:3) = 0;
imgzy(y, :, 1) = 255;
imgzy(y, :, 2:3) = 0;

 imgzy(:, 1,:) = 0;
 imgzy(1, :,:) = 0;
  imgzy(end, :,:) = 0;
 imgzy(:, end,:) = 0;

img = [imgxy imgzy];

nonimg = ones( [size(imgxz,1), size(imgzy,2), 3])*255;
img = [img; [imgxz nonimg]];
% Put some text


text_str = cell(3,1);
conf_val = [85.212 98.76 78.342]; 
txt_str{1} = 'xy';
txt_str{3} = 'zy';
txt_str{2} = 'xz';
position = [1 1;1 size(imgxy,1)+2;size(imgxy,2)+2 1]; 
box_color = {'black','black','black'};
img = insertText(img,position,txt_str,'FontSize',15,'BoxColor',...
    box_color,'BoxOpacity',0.4,'TextColor','white');


