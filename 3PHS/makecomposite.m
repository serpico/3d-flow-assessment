i1 = readtiff('/media/smanandh/Sandeep/cell19_8-12/1_CAM01_000008.tif');
i2 = readtiff('/media/smanandh/Sandeep/cell19_8-12/1_CAM01_000012.tif');

i1 = imresize3(i1, [size(i1,1), size(i1,2), size(i1,3)*1]);
i2 = imresize3(i2, [size(i1,1), size(i1,2), size(i1,3)]);
iyz1 = squeeze(i1(:,144, :));
iyz2 = squeeze(i2(:,144, :));
imshowpair(iyz1, iyz2);
export_fig('/media/smanandh/Sandeep/cell19_8-12/yz.png');
ixz1 = squeeze(i1(150,:, :))';
ixz2 = squeeze(i2(150,:, :))';
imshowpair(ixz1, ixz2);
export_fig('/media/smanandh/Sandeep/cell19_8-12/xz.png');

ixy1 = i1(:,:,29);
ixy2 = i2(:,:,29);

imshowpair(ixy1, ixy2);
export_fig('/media/smanandh/Sandeep/cell19_8-12/xyam.png');